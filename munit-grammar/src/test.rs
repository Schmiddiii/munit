use crate::grammar::{self, *};

use rust_sitter::{errors::ParseError, Spanned};

fn unspanned<T>(t: T) -> Spanned<T> {
    Spanned {
        value: t,
        span: (0, 0),
    }
}

fn eval(expr: Spanned<Expr>) -> Grammar {
    Grammar::Eval(SpannedExpr(expr))
}

#[test]
fn parse_numeric() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Integer(10),
            unit: None
        }))))
    );
    Ok(())
}

#[test]
fn parse_numeric_float() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10.75")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Floating(10.75),
            unit: None
        }))))
    );
    Ok(())
}

#[test]
fn parse_numeric_unit() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10unit")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Integer(10),
            unit: Some("unit".into())
        }))))
    );
    Ok(())
}

#[test]
fn parse_numeric_unit_with_whitespace() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10 unit")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Integer(10),
            unit: Some("unit".into())
        }))))
    );
    Ok(())
}

#[test]
fn parse_numeric_unit_float() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10.75unit")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Floating(10.75),
            unit: Some("unit".into())
        }))))
    );
    Ok(())
}

#[test]
fn parse_add() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10unit + 20.5other")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Add(
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Integer(10),
                    unit: Some("unit".into())
                }
            ))))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Floating(20.5),
                    unit: Some("other".into())
                }
            )))))
        )))
    );
    Ok(())
}

#[test]
fn parse_sub() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10unit - 20.5other")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Sub(
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Integer(10),
                    unit: Some("unit".into())
                }
            ))))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Floating(20.5),
                    unit: Some("other".into())
                }
            )))))
        )))
    );
    Ok(())
}

#[test]
fn parse_mul() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10unit * 20.5other")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Mul(
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Integer(10),
                    unit: Some("unit".into())
                }
            ))))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Floating(20.5),
                    unit: Some("other".into())
                }
            )))))
        )))
    );
    Ok(())
}

#[test]
fn parse_div() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10unit / 20.5other")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Div(
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Integer(10),
                    unit: Some("unit".into())
                }
            ))))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Floating(20.5),
                    unit: Some("other".into())
                }
            )))))
        )))
    );
    Ok(())
}

#[test]
fn parse_bracketted() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("(10unit + 5other) / 20.5other")?
        .grammar
        .unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Div(
            Box::new(SpannedExpr(unspanned(Expr::Bracketed(
                (),
                Box::new(SpannedExpr(unspanned(Expr::Add(
                    Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                        NumberWithUnit {
                            number: Number::Integer(10),
                            unit: Some("unit".into())
                        }
                    ))))),
                    (),
                    Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                        NumberWithUnit {
                            number: Number::Integer(5),
                            unit: Some("other".into())
                        }
                    )))))
                )))),
                (),
            )))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Floating(20.5),
                    unit: Some("other".into())
                }
            )),)))
        )))
    );
    Ok(())
}

#[test]
fn parse_multiple_operations() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("5.0unit + 3unit * 10unit / 20.5other - 1other")?
        .grammar
        .unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Sub(
            Box::new(SpannedExpr(unspanned(Expr::Add(
                Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                    NumberWithUnit {
                        number: Number::Floating(5.0),
                        unit: Some("unit".into())
                    }
                ))))),
                (),
                Box::new(SpannedExpr(unspanned(Expr::Div(
                    Box::new(SpannedExpr(unspanned(Expr::Mul(
                        Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                            NumberWithUnit {
                                number: Number::Integer(3),
                                unit: Some("unit".into())
                            }
                        ))))),
                        (),
                        Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                            NumberWithUnit {
                                number: Number::Integer(10),
                                unit: Some("unit".into())
                            }
                        )))))
                    )))),
                    (),
                    Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                        NumberWithUnit {
                            number: Number::Floating(20.5),
                            unit: Some("other".into())
                        }
                    ))))),
                )))),
            )))),
            (),
            Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                NumberWithUnit {
                    number: Number::Integer(1),
                    unit: Some("other".into())
                }
            )))))
        )))
    );
    Ok(())
}

#[test]
fn parse_function() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("function(10unit, 20unit + 3other)")?
        .grammar
        .unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Function(
            "function".to_owned(),
            (),
            vec![
                SpannedExpr(unspanned(Expr::Number(unspanned(NumberWithUnit {
                    number: Number::Integer(10),
                    unit: Some("unit".into())
                })))),
                SpannedExpr(unspanned(Expr::Add(
                    Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                        NumberWithUnit {
                            number: Number::Integer(20),
                            unit: Some("unit".into())
                        }
                    ))))),
                    (),
                    Box::new(SpannedExpr(unspanned(Expr::Number(unspanned(
                        NumberWithUnit {
                            number: Number::Integer(3),
                            unit: Some("other".into())
                        }
                    )))))
                )))
            ],
            (),
        )))
    );
    Ok(())
}

#[test]
fn parse_complex_unit() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("10 km pi^2 | s^3 e")?.grammar.unwrap();
    assert_eq!(
        parsed,
        eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
            number: Number::Integer(10),
            unit: Some(Unit {
                numerator: UnitList {
                    units: vec![
                        UnitPart {
                            unit: "km".to_owned(),
                            power: None
                        },
                        UnitPart {
                            unit: "pi".to_owned(),
                            power: Some(UnitPower { _sym: (), num: 2 })
                        }
                    ]
                },
                denumerator: Some(DenumeratorPart {
                    _sym: (),
                    units: UnitList {
                        units: vec![
                            UnitPart {
                                unit: "s".to_owned(),
                                power: Some(UnitPower { _sym: (), num: 3 })
                            },
                            UnitPart {
                                unit: "e".to_owned(),
                                power: None
                            }
                        ]
                    }
                })
            })
        }))))
    );
    Ok(())
}

#[test]
fn parse_command_exit() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse(":exit")?.grammar.unwrap();
    assert_eq!(parsed, Grammar::Command((), Command::Exit(())));
    Ok(())
}

#[test]
fn parse_command_convert() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse(":convert 1s = 5")?.grammar.unwrap();
    assert_eq!(
        parsed,
        Grammar::Command(
            (),
            Command::Conversion(
                (),
                SpannedExpr(unspanned(Expr::Number(unspanned(NumberWithUnit {
                    number: Number::Integer(1),
                    unit: Some("s".into())
                })))),
                (),
                SpannedExpr(unspanned(Expr::Number(unspanned(NumberWithUnit {
                    number: Number::Integer(5),
                    unit: None
                }))))
            )
        )
    );
    Ok(())
}

#[test]
fn parse_comment() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("# Hello World!")?;
    assert_eq!(
        parsed,
        GrammarWithComment {
            grammar: None,
            comment: Some(Comment {
                _hash: (),
                comment: "Hello World!".to_string()
            })
        }
    );
    Ok(())
}

#[test]
fn parse_comment_with_grammar() -> Result<(), Vec<ParseError>> {
    let parsed = grammar::parse("1 # Hello World!")?;
    assert_eq!(
        parsed,
        GrammarWithComment {
            grammar: Some(eval(unspanned(Expr::Number(unspanned(NumberWithUnit {
                number: Number::Integer(1),
                unit: None
            }))))),
            comment: Some(Comment {
                _hash: (),
                comment: "Hello World!".to_string()
            })
        }
    );
    Ok(())
}
