#[rust_sitter::grammar("munit")]
mod grammar {
    use rust_sitter::Spanned;

    #[rust_sitter::language]
    #[derive(Debug, PartialEq)]
    pub struct GrammarWithComment {
        pub grammar: Option<Grammar>,
        pub comment: Option<Comment>,
    }

    #[derive(Debug, PartialEq)]
    pub enum Grammar {
        Eval(SpannedExpr),
        Command(#[rust_sitter::leaf(text = ":")] (), Command),
    }

    impl From<GrammarWithComment> for Option<Grammar> {
        fn from(g: GrammarWithComment) -> Option<Grammar> {
            g.grammar
        }
    }

    #[derive(PartialEq, Debug)]
    pub struct Comment {
        #[rust_sitter::leaf(pattern = r"#(\s)")]
        pub _hash: (),
        #[rust_sitter::leaf(pattern = r".*", transform = |v| v.to_string())]
        pub comment: String,
    }

    #[derive(Debug, PartialEq)]
    pub enum Command {
        Exit(#[rust_sitter::leaf(text = "exit")] ()),
        Conversion(
            #[rust_sitter::leaf(text = "convert")] (),
            SpannedExpr,
            #[rust_sitter::leaf(text = "=")] (),
            SpannedExpr,
        ),
        Like(#[rust_sitter::leaf(text = "like")] (), UnitPart),
        Dislike(#[rust_sitter::leaf(text = "dislike")] (), UnitPart),
    }

    #[derive(Debug)]
    pub struct SpannedExpr(pub Spanned<Expr>);

    impl PartialEq for SpannedExpr {
        fn eq(&self, other: &Self) -> bool {
            match (&self.0.value, &other.0.value) {
                (Expr::Number(n1), Expr::Number(n2)) => n1.value == n2.value,
                (Expr::Add(a1, _, a2), Expr::Add(b1, _, b2)) => a1 == b1 && a2 == b2,
                (Expr::Sub(a1, _, a2), Expr::Sub(b1, _, b2)) => a1 == b1 && a2 == b2,
                (Expr::Mul(a1, _, a2), Expr::Mul(b1, _, b2)) => a1 == b1 && a2 == b2,
                (Expr::Div(a1, _, a2), Expr::Div(b1, _, b2)) => a1 == b1 && a2 == b2,
                (Expr::Bracketed(_, a, _), Expr::Bracketed(_, b, _)) => a == b,
                (Expr::Function(a1, _, a2, _), Expr::Function(b1, _, b2, _)) => {
                    a1 == b1 && a2 == b2
                }
                (_, _) => false,
            }
        }
    }

    // TODO: Power
    #[derive(Debug)]
    pub enum Expr {
        Number(Spanned<NumberWithUnit>),
        #[rust_sitter::prec_left(1)]
        Add(
            Box<SpannedExpr>,
            #[rust_sitter::leaf(text = "+")] (),
            Box<SpannedExpr>,
        ),
        #[rust_sitter::prec_left(1)]
        Sub(
            Box<SpannedExpr>,
            #[rust_sitter::leaf(text = "-")] (),
            Box<SpannedExpr>,
        ),
        #[rust_sitter::prec_left(10)]
        Mul(
            Box<SpannedExpr>,
            #[rust_sitter::leaf(text = "*")] (),
            Box<SpannedExpr>,
        ),
        #[rust_sitter::prec_left(10)]
        Div(
            Box<SpannedExpr>,
            #[rust_sitter::leaf(text = "/")] (),
            Box<SpannedExpr>,
        ),
        #[rust_sitter::prec(1000)]
        Bracketed(
            #[rust_sitter::leaf(text = "(")] (),
            Box<SpannedExpr>,
            #[rust_sitter::leaf(text = ")")] (),
        ),
        Function(
            #[rust_sitter::leaf(pattern = r"[a-zA-Z_]+", transform = |v| v.to_string())] String,
            #[rust_sitter::leaf(text = "(")] (),
            #[rust_sitter::delimited(
                #[rust_sitter::leaf(text = ",")]
                ()
            )]
            Vec<SpannedExpr>,
            #[rust_sitter::leaf(text = ")")] (),
        ),
    }

    #[derive(PartialEq, Debug)]
    pub enum Number {
        Integer(#[rust_sitter::leaf(pattern = r"\d+", transform = |v| v.parse().unwrap())] i64),
        Floating(
            #[rust_sitter::leaf(pattern = r"\d+\.\d+", transform = |v| v.parse().unwrap())] f64,
        ),
    }

    #[derive(PartialEq, Debug)]
    pub struct NumberWithUnit {
        pub number: Number,
        pub unit: Option<Unit>,
    }

    #[derive(PartialEq, Debug)]
    pub struct UnitPower {
        #[rust_sitter::leaf(text = "^")]
        pub _sym: (),
        #[rust_sitter::leaf(pattern = r"\d+", transform = |v| v.parse().unwrap())]
        pub num: i64,
    }

    #[derive(PartialEq, Debug)]
    pub struct UnitPart {
        #[rust_sitter::leaf(pattern = r"[a-zA-Z_]+", transform = |v| v.to_string())]
        pub unit: String,
        pub power: Option<UnitPower>,
    }

    #[derive(PartialEq, Debug)]
    pub struct UnitList {
        #[rust_sitter::repeat(non_empty = true)]
        pub units: Vec<UnitPart>,
    }

    #[derive(PartialEq, Debug)]
    pub struct DenumeratorPart {
        // TODO: Should be /
        #[rust_sitter::leaf(text = "|")]
        pub _sym: (),
        pub units: UnitList,
    }

    #[derive(PartialEq, Debug)]
    pub struct Unit {
        pub numerator: UnitList,
        pub denumerator: Option<DenumeratorPart>,
    }

    #[rust_sitter::extra]
    pub struct Whitespace {
        #[rust_sitter::leaf(pattern = r"\s")]
        _whitespace: (),
    }
}

impl From<&str> for grammar::Unit {
    fn from(s: &str) -> Self {
        Self {
            numerator: grammar::UnitList {
                units: vec![grammar::UnitPart {
                    unit: s.to_owned(),
                    power: None,
                }],
            },
            denumerator: None,
        }
    }
}

pub use grammar::*;

#[cfg(test)]
mod test;
