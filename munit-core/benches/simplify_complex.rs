use criterion::{black_box, criterion_group, criterion_main, Criterion};
use munit_core::{
    Conversion, HeuristicBadnessEvaluator, Preferences, RepeatedConversionSimplifier, Simplifier,
    Unit,
};

mod perf;

fn simplify_more_complex() {
    let number = 2.0 * Unit::base_power("a", 4);
    let goal = 10.0 * Unit::base_power("d", 1);
    let conversions = &[
        Conversion {
            from: 1.0 * Unit::base_power("a", 3),
            to: 0.5 * Unit::base_power("b", 1),
        },
        Conversion {
            from: 1.0 * Unit::base_power("a", 1) * Unit::base_power("b", 1),
            to: 2.0 * Unit::base_power("c", 1),
        },
        Conversion {
            from: 1.0 * Unit::base_power("c", 1),
            to: 5.0 * Unit::base_power("d", 1),
        },
    ];
    let preferences = &[Preferences::Like(Unit::base_power("d", 1))];

    assert_eq!(
        goal,
        RepeatedConversionSimplifier::<HeuristicBadnessEvaluator>::simplify(
            black_box(number),
            black_box(conversions),
            black_box(preferences),
        )
    );
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("simplify_complex", |b| b.iter(simplify_more_complex));
}

fn profiled() -> Criterion {
    Criterion::default().with_profiler(perf::FlamegraphProfiler::new(100))
}

criterion_group! { name = benches; config = profiled(); targets = criterion_benchmark }
// criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
