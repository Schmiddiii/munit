use rand::{rngs::StdRng, Rng, SeedableRng};

use crate::{BadnessEvaluator, NumberWithUnit, Preferences};

pub struct Conversion {
    pub from: NumberWithUnit,
    pub to: NumberWithUnit,
}

impl Conversion {
    pub fn apply(&self, number: NumberWithUnit) -> NumberWithUnit {
        number / (&self.from) * (&self.to)
    }
}

pub trait Simplifier<B: BadnessEvaluator> {
    fn simplify(
        number: NumberWithUnit,
        conversions: &[Conversion],
        preferences: &[Preferences],
    ) -> NumberWithUnit;
}

const SIZE_KEEP: usize = 10;
const NUMBER_USE: usize = 2;
const ITERATIONS: usize = 10 * SIZE_KEEP;
const RANDOM_SEED: u64 = 123456;
pub struct RepeatedConversionSimplifier<B>(std::marker::PhantomData<B>);

impl<B: BadnessEvaluator> Simplifier<B> for RepeatedConversionSimplifier<B> {
    fn simplify(
        number: NumberWithUnit,
        conversions: &[Conversion],
        preferences: &[Preferences],
    ) -> NumberWithUnit {
        let mut random = StdRng::seed_from_u64(RANDOM_SEED);

        let mut memory = Vec::with_capacity(SIZE_KEEP);
        memory.push((B::evaluate(&number, preferences), number));

        for _ in 1..ITERATIONS {
            let (current_worst_badness, _) = &memory[memory.len() - 1];

            let (_, num) = memory[random.gen_range(0..memory.len())].clone();
            let mut conversions: Vec<_> = conversions
                .iter()
                .flat_map(|c| {
                    num.all_splits()
                        .into_iter()
                        .map(|(s1, s2)| c.apply(s1) + s2)
                })
                .map(|n| (B::evaluate(&n, preferences), n))
                .filter(|(b, _)| b < current_worst_badness)
                .collect();
            conversions.sort_by_key(|n| n.0);

            let mut keeps = Vec::with_capacity(NUMBER_USE);
            if !conversions.is_empty() {
                keeps.push(conversions.swap_remove(0));
            }
            while keeps.len() < NUMBER_USE && !conversions.is_empty() {
                keeps.push(conversions.swap_remove(random.gen_range(0..conversions.len())));
            }

            memory.truncate(SIZE_KEEP - keeps.len());
            while let Some(num) = keeps.pop() {
                memory.insert(
                    memory
                        .binary_search_by_key(&num.0, |n| n.0)
                        .unwrap_or_else(|e| e),
                    num,
                );
            }
        }

        memory.swap_remove(0).1
    }
}

#[cfg(test)]
mod test {
    use crate::{HeuristicBadnessEvaluator, Unit};

    use super::*;

    #[test]
    fn convert() {
        let number = 2.0 * Unit::base_power("a", 3);
        let goal = 1.0 * Unit::base_power("b", 1);
        let conversion = Conversion {
            from: 1.0 * Unit::base_power("a", 3),
            to: 0.5 * Unit::base_power("b", 1),
        };

        assert_eq!(goal, conversion.apply(number));
    }

    #[test]
    fn simplify_trivial() {
        let number = 2.0 * Unit::base_power("a", 3);
        let goal = 1.0 * Unit::base_power("b", 1);
        let conversion = Conversion {
            from: 1.0 * Unit::base_power("a", 3),
            to: 0.5 * Unit::base_power("b", 1),
        };

        assert_eq!(
            goal,
            RepeatedConversionSimplifier::<HeuristicBadnessEvaluator>::simplify(
                number,
                &[conversion],
                &[]
            )
        );
    }

    #[test]
    fn simplify_more_complex() {
        let number = 2.0 * Unit::base_power("a", 4);
        let goal = 10.0 * Unit::base_power("d", 1);
        let conversions = &[
            Conversion {
                from: 1.0 * Unit::base_power("a", 3),
                to: 0.5 * Unit::base_power("b", 1),
            },
            Conversion {
                from: 1.0 * Unit::base_power("a", 1) * Unit::base_power("b", 1),
                to: 2.0 * Unit::base_power("c", 1),
            },
            Conversion {
                from: 1.0 * Unit::base_power("c", 1),
                to: 5.0 * Unit::base_power("d", 1),
            },
        ];
        let preferences = &[Preferences::Like(Unit::base_power("d", 1))];

        assert_eq!(
            goal,
            RepeatedConversionSimplifier::<HeuristicBadnessEvaluator>::simplify(
                number,
                conversions,
                preferences,
            )
        );
    }

    #[test]
    fn simplify_apply_to_single_term() {
        let number = 1.0 * Unit::base_power("i", 2) + 1.0 * Unit::none();
        let goal = NumberWithUnit::zero();
        let conversions = &[Conversion {
            from: 1.0 * Unit::base_power("i", 2),
            to: -1.0 * Unit::none(),
        }];
        let preferences = &[];

        assert_eq!(
            goal,
            RepeatedConversionSimplifier::<HeuristicBadnessEvaluator>::simplify(
                number,
                conversions,
                preferences,
            )
        );
    }

    #[test]
    fn simplify_complex_numbers() {
        let number = (1.0 * Unit::base("i") + 2.0 * Unit::none())
            * (3.0 * Unit::base("i") + 4.0 * Unit::none());
        dbg!(&number);
        let goal = 10.0 * Unit::base("i") + 5.0 * Unit::none();
        let conversions = &[Conversion {
            from: 1.0 * Unit::base_power("i", 2),
            to: -1.0 * Unit::none(),
        }];
        let preferences = &[];

        assert_eq!(
            goal,
            RepeatedConversionSimplifier::<HeuristicBadnessEvaluator>::simplify(
                number,
                conversions,
                preferences,
            )
        );
    }
}
