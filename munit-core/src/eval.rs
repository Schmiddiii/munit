use std::{collections::HashMap, error::Error, fmt::Display};

use crate::NumberWithUnit;

// TODO: Maybe specify here how many arguments this accepts.
// This moves "Illegal arguent count" outside of function evaluation.
type Function = dyn Fn(Vec<NumberWithUnit>) -> Result<NumberWithUnit, EvalError>;

type Span = (usize, usize);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Spanned<T> {
    pub value: T,
    pub span: Span,
}

impl<T: Error> Error for Spanned<T> {}

impl<T: Error> Display for Spanned<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} at {} to {}",
            self.value,
            self.span.0,
            self.span.1 - 1
        )
    }
}

#[derive(Debug, thiserror::Error, PartialEq, Eq)]
pub enum EvalError {
    #[error("function `{0}` was not found")]
    FunctionNotFound(String),
    #[error("function `{0}` expected `{1}` argument, but got `{2}`")]
    IncorrectArgumentCount(String, usize, usize),
    #[error("divide by zero")]
    DivideByZeroError,
}

pub struct EvalContext {
    functions: HashMap<String, Box<Function>>,
}

impl Default for EvalContext {
    fn default() -> Self {
        let mut functions: HashMap<String, Box<Function>> = HashMap::new();
        functions.insert(
            "+".to_string(),
            Box::new(|v| Ok(v.into_iter().fold(NumberWithUnit::zero(), |v1, v2| v1 + v2))),
        );
        functions.insert(
            "-".to_string(),
            Box::new(|mut v| {
                if v.len() != 2 {
                    Err(EvalError::IncorrectArgumentCount(
                        "-".to_string(),
                        2,
                        v.len(),
                    ))
                } else {
                    Ok(v.remove(0) - v.remove(0))
                }
            }),
        );
        functions.insert(
            "*".to_string(),
            Box::new(|v| Ok(v.into_iter().fold(NumberWithUnit::one(), |v1, v2| v1 * v2))),
        );
        functions.insert(
            "/".to_string(),
            Box::new(|mut v| {
                if v.len() != 2 {
                    Err(EvalError::IncorrectArgumentCount(
                        "/".to_string(),
                        2,
                        v.len(),
                    ))
                } else if v[1] == NumberWithUnit::zero() {
                    Err(EvalError::DivideByZeroError)
                } else {
                    Ok(v.remove(0) / v.remove(0))
                }
            }),
        );
        Self { functions }
    }
}

impl EvalContext {
    pub fn eval(&self, mut tree: EvalTree) -> Result<NumberWithUnit, Spanned<EvalError>> {
        while let EvalTree::Function(_) = &tree {
            tree = self.eval_once(tree)?;
        }
        if let EvalTree::Number(v) = tree {
            Ok(v.value)
        } else {
            panic!("impossible")
        }
    }
    pub fn eval_once(&self, tree: EvalTree) -> Result<EvalTree, Spanned<EvalError>> {
        match tree {
            EvalTree::Number(v) => Ok(EvalTree::Number(v)),
            EvalTree::Function(v) => {
                let (name, mut args) = v.value;
                match args
                    .iter()
                    .enumerate()
                    .find(|(_, v)| matches!(v, EvalTree::Function(_)))
                {
                    // Children already all evaluated. Evaluate function
                    None => {
                        let args_num = args
                            .into_iter()
                            .map(|a| {
                                if let EvalTree::Number(v) = a {
                                    v.value
                                } else {
                                    panic!("impossible")
                                }
                            })
                            .collect();
                        let function = self.functions.get(&name).ok_or(Spanned {
                            value: EvalError::FunctionNotFound(name),
                            span: v.span,
                        })?;
                        function(args_num)
                            .map_err(|e| Spanned {
                                value: e,
                                span: v.span,
                            })
                            .map(|n| {
                                EvalTree::Number(Spanned {
                                    value: n,
                                    span: v.span,
                                })
                            })
                    }
                    Some((i, _)) => {
                        args[i] = self.eval_once(args[i].clone())?;
                        Ok(EvalTree::Function(Spanned {
                            value: (name, args),
                            span: v.span,
                        }))
                    }
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum EvalTree {
    Number(Spanned<NumberWithUnit>),
    Function(Spanned<(String, Vec<EvalTree>)>),
}

#[cfg(test)]
mod test {
    use crate::Unit;

    use super::*;

    fn unspanned<T>(v: T) -> Spanned<T> {
        Spanned {
            value: v,
            span: (0, 0),
        }
    }

    fn number(n: NumberWithUnit) -> EvalTree {
        EvalTree::Number(unspanned(n))
    }

    fn function(n: &str, args: Vec<EvalTree>) -> EvalTree {
        EvalTree::Function(unspanned((n.to_string(), args)))
    }

    #[test]
    fn eval_simple() -> Result<(), Box<dyn std::error::Error>> {
        let tree = function(
            "+",
            vec![number(1.0 * Unit::base("s")), number(2.0 * Unit::base("s"))],
        );
        let eval = EvalContext::default();

        assert_eq!(3.0 * Unit::base("s"), eval.eval(tree)?);
        Ok(())
    }

    #[test]
    fn eval_complex() -> Result<(), Box<dyn std::error::Error>> {
        let tree = function(
            "+",
            vec![
                function(
                    "*",
                    vec![number(3.0 * Unit::base("s")), number(2.0 * Unit::base("m"))],
                ),
                number(1.0 * Unit::base("s") * Unit::base("m")),
            ],
        );
        let eval = EvalContext::default();

        assert_eq!(7.0 * Unit::base("s") * Unit::base("m"), eval.eval(tree)?);
        Ok(())
    }

    #[test]
    fn eval_too_many_arguments() -> Result<(), Box<dyn std::error::Error>> {
        let tree = EvalTree::Function(Spanned {
            value: (
                "/".to_owned(),
                vec![
                    number(3.0 * Unit::base("s")),
                    number(2.0 * Unit::base("m")),
                    number(1.0 * Unit::base("m")),
                ],
            ),
            span: (10, 20),
        });
        let eval = EvalContext::default();

        assert_eq!(
            Err(Spanned {
                value: EvalError::IncorrectArgumentCount("/".to_owned(), 2, 3),
                span: (10, 20)
            }),
            eval.eval(tree)
        );
        Ok(())
    }

    #[test]
    fn eval_function_not_found() -> Result<(), Box<dyn std::error::Error>> {
        let tree = EvalTree::Function(Spanned {
            value: (
                "asd".to_owned(),
                vec![
                    number(3.0 * Unit::base("s")),
                    number(2.0 * Unit::base("m")),
                    number(1.0 * Unit::base("m")),
                ],
            ),
            span: (10, 20),
        });
        let eval = EvalContext::default();

        assert_eq!(
            Err(Spanned {
                value: EvalError::FunctionNotFound("asd".to_string()),
                span: (10, 20)
            }),
            eval.eval(tree)
        );
        Ok(())
    }

    #[test]
    fn eval_div_by_zero() -> Result<(), Box<dyn std::error::Error>> {
        let tree = EvalTree::Function(Spanned {
            value: (
                "/".to_owned(),
                vec![
                    number(3.0 * Unit::base("s")),
                    function(
                        "*",
                        vec![
                            number(NumberWithUnit::zero()),
                            number(1.0 * Unit::base("s")),
                        ],
                    ),
                ],
            ),
            span: (10, 20),
        });
        let eval = EvalContext::default();

        assert_eq!(
            Err(Spanned {
                value: EvalError::DivideByZeroError,
                span: (10, 20)
            }),
            eval.eval(tree)
        );
        Ok(())
    }
}
