use std::collections::HashSet;

use crate::{NumberWithUnit, Unit};

pub enum Preferences {
    Like(Unit),
    Dislike(Unit),
}

pub trait BadnessEvaluator {
    fn evaluate(number: &NumberWithUnit, preferences: &[Preferences]) -> isize;
    fn prefers(
        number1: &NumberWithUnit,
        number2: &NumberWithUnit,
        preferences: &[Preferences],
    ) -> bool {
        Self::evaluate(number1, preferences) < Self::evaluate(number2, preferences)
    }
}

const BADNESS_NONCONSTANT_DENOMINATOR: isize = 500;
const BADNESS_PER_SEPARATE_UNIT: isize = 100;
const BADNESS_PER_POWER: isize = 10;
const BADNESS_PREFERENCES_LIKE: isize = -50;
const BADNESS_PREFERENCES_DISLIKE: isize = 50;
const BADNESS_PER_DECIMAL_UNIT: isize = 5;
const BADNESS_NEGATIVE: isize = 5;

pub struct HeuristicBadnessEvaluator;

impl BadnessEvaluator for HeuristicBadnessEvaluator {
    fn evaluate(number: &NumberWithUnit, preferences: &[Preferences]) -> isize {
        let liked = preferences
            .iter()
            .filter_map(|u| {
                if let Preferences::Like(u) = u {
                    Some(u)
                } else {
                    None
                }
            })
            .collect::<HashSet<&Unit>>();
        let disliked = preferences
            .iter()
            .filter_map(|u| {
                if let Preferences::Dislike(u) = u {
                    Some(u)
                } else {
                    None
                }
            })
            .collect::<HashSet<&Unit>>();

        let mut result = 0;

        if number.denominator.len() != 1 {
            result += BADNESS_NONCONSTANT_DENOMINATOR;
        }

        result += (number.numerator.len() as isize) * BADNESS_PER_SEPARATE_UNIT;
        result += (number.denominator.len() as isize) * BADNESS_PER_SEPARATE_UNIT;

        let mut all = number.numerator.iter().collect::<Vec<_>>();
        all.extend(number.denominator.iter());

        for (u, v) in all.into_iter() {
            result += (u.powers() as isize) * BADNESS_PER_POWER;
            result += liked
                .iter()
                .map(|l| {
                    if u.contains(l) {
                        BADNESS_PREFERENCES_LIKE
                    } else {
                        0
                    }
                })
                .sum::<isize>();
            result += disliked
                .iter()
                .map(|l| {
                    if u.contains(l) {
                        BADNESS_PREFERENCES_DISLIKE
                    } else {
                        0
                    }
                })
                .sum::<isize>();

            if v < &0.0 {
                result += BADNESS_NEGATIVE;
            }

            if v.abs() < 1.0 {
                let decimals = -v.abs().log10() as isize;
                result += decimals * BADNESS_PER_DECIMAL_UNIT;
            } else {
                let decimals = v.abs().log10() as isize;
                result += decimals * BADNESS_PER_DECIMAL_UNIT;
            }
        }

        result
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn prefer_less_separate() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(1.0 * Unit::base("s")),
            &(1.0 * Unit::none() + 1.0 * Unit::base("s")),
            &[]
        ));
    }

    #[test]
    fn prefer_less_power() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(1.0 * Unit::none()),
            &(1.0 * Unit::base("s")),
            &[]
        ));
    }

    #[test]
    fn prefer_no_zero_dot() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(1.0 * Unit::none()),
            &(0.1 * Unit::none()),
            &[]
        ));
    }

    #[test]
    fn prefer_like() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(1.0 * Unit::base("s")),
            &(1.0 * Unit::none()),
            &[Preferences::Like(Unit::base("s"))]
        ));
    }

    #[test]
    fn prefer_dislike() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(1.0 * Unit::none()),
            &(1.0 * Unit::base("s")),
            &[Preferences::Dislike(Unit::base("s"))]
        ));
    }

    #[test]
    fn prefer_less_decimals() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(10.0 * Unit::none()),
            &(100.0 * Unit::none()),
            &[]
        ));
    }

    #[test]
    fn prefer_non_negative() {
        assert!(HeuristicBadnessEvaluator::prefers(
            &(10.0 * Unit::base("i")),
            &(-10.0 * Unit::base_power("i", -1)),
            &[]
        ));
    }
}
