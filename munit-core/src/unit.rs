use std::fmt::Display;
use std::hash::Hash;
use std::ops::Div;
use std::{collections::HashMap, ops::Mul};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Unit(HashMap<String, i64>);

impl Display for Unit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let needs_power_everywhere = self.0.len() > 1;

        let result = self
            .0
            .iter()
            .map(|(s, &k)| {
                if k > 1 || needs_power_everywhere {
                    format!("{}^{}", s, k)
                } else {
                    s.clone()
                }
            })
            .collect::<String>();
        f.write_str(&result)
    }
}

impl Hash for Unit {
    // XXX: Think about a better hasher?
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let mut v: Vec<(&String, &i64)> = self.0.iter().collect();
        v.sort_by_key(|(k, _)| *k);
        v.hash(state)
    }
}

impl Unit {
    pub fn none() -> Self {
        Unit(HashMap::new())
    }

    pub fn base_power<S: AsRef<str>>(unit: S, power: i64) -> Self {
        if power == 0 {
            Self::none()
        } else {
            Unit(HashMap::from([(unit.as_ref().to_owned(), power)]))
        }
    }

    pub fn base<S: AsRef<str>>(unit: S) -> Self {
        Self::base_power(unit, 1)
    }

    pub fn pow(mut self, pow: i64) -> Self {
        self.0.values_mut().for_each(|v| {
            *v *= pow;
        });
        self
    }

    pub fn inverse(self) -> Self {
        self.pow(-1)
    }

    /// Removes all `0`-powers.
    fn simplify(&mut self) {
        self.0.retain(|_, v| *v != 0)
    }

    pub(crate) fn powers(&self) -> u64 {
        self.0.values().map(|v| v.unsigned_abs()).sum()
    }

    pub(crate) fn contains(&self, other: &Unit) -> bool {
        for (k, v) in &other.0 {
            if self.0.get(k).unwrap_or(&0).abs() < v.abs() {
                return false;
            }
        }

        true
    }
}

impl Mul for Unit {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        self * (&other)
    }
}

impl Mul<&Unit> for Unit {
    type Output = Self;

    fn mul(mut self, other: &Self) -> Self::Output {
        other.0.iter().for_each(|(u, p)| {
            self.0
                .entry(u.to_string())
                .and_modify(|e| *e += p)
                .or_insert(*p);
        });
        self.simplify();
        self
    }
}

impl Div for Unit {
    type Output = Unit;

    // Not sure why this is disallowed here.
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, rhs: Self) -> Self::Output {
        self / (&rhs)
    }
}

impl Div<&Unit> for Unit {
    type Output = Unit;

    // Not sure why this is disallowed here.
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, rhs: &Self) -> Self::Output {
        self * rhs.clone().inverse()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn base_construction() {
        assert_eq!(Unit(HashMap::from([("m".to_string(), 1)])), Unit::base("m"));
        assert_eq!(Unit(HashMap::from([])), Unit::base_power("m", 0));
        assert_eq!(
            Unit(HashMap::from([("km".to_string(), 5)])),
            Unit::base_power("km", 5)
        );
    }

    #[test]
    fn inversion() {
        assert_eq!(
            Unit(HashMap::from([("km".to_string(), -5)])),
            Unit::base_power("km", 5).inverse()
        );
    }

    #[test]
    fn power() {
        assert_eq!(
            Unit(HashMap::from([("km".to_string(), 4)])),
            Unit::base("km").pow(4)
        );
    }

    #[test]
    fn multiplication_simple() {
        assert_eq!(
            Unit(HashMap::from([("km".to_string(), 5), ("s".to_string(), 3)])),
            Unit::base_power("km", 5) * Unit::base_power("s", 3)
        );
    }

    #[test]
    fn multiplication_overlap() {
        assert_eq!(
            Unit(HashMap::from([("km".to_string(), 8)])),
            Unit::base_power("km", 5) * Unit::base_power("km", 3)
        );
    }

    #[test]
    fn multiplication_complex() {
        assert_eq!(
            Unit(HashMap::from([
                ("km".to_string(), 3),
                ("s".to_string(), -1),
                ("unit".to_string(), -5)
            ])),
            Unit::base_power("s", 9)
                * Unit::base_power("km", 5)
                * Unit::base_power("unit", -8)
                * Unit::base_power("km", -2)
                * Unit::base_power("s", -10)
                * Unit::base("falls")
                * Unit::base("falls").inverse()
                * Unit::base_power("unit", 3)
        );
    }
}
