use crate::Unit;

use std::{
    collections::HashMap,
    fmt::Display,
    ops::{Add, Div, Mul, Neg, Sub},
};

#[derive(Debug, PartialEq, Clone)]
pub struct NumberWithUnit {
    pub(crate) numerator: HashMap<Unit, f64>,
    pub(crate) denominator: HashMap<Unit, f64>,
}

fn format_number_unit(number: f64, unit: &Unit) -> String {
    if number.floor() == number {
        format!("{:.0}{}", number, unit)
    } else {
        format!("{:.2}{}", number, unit)
    }
}

impl Display for NumberWithUnit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let needs_denom = self.has_denominator();
        let needs_brackets = needs_denom && self.numerator.len() > 1;

        let numer = self
            .numerator
            .iter()
            .map(|(u, v)| format_number_unit(*v, u))
            .collect::<Vec<_>>()
            .join(" + ");
        let denom = self
            .denominator
            .iter()
            .map(|(u, v)| format_number_unit(*v, u))
            .collect::<Vec<_>>()
            .join(" + ");

        if needs_brackets {
            write!(f, "({}) / ({})", numer, denom)
        } else if needs_denom {
            write!(f, "{} / ({})", numer, denom)
        } else {
            f.write_str(&numer)
        }
    }
}

impl NumberWithUnit {
    fn from_hash_numerator(m: HashMap<Unit, f64>) -> Self {
        let mut res = Self {
            numerator: m,
            denominator: HashMap::from([(Unit::none(), 1.0)]),
        };
        res.simplify();
        res
    }
    pub fn zero() -> Self {
        Self::from_hash_numerator(HashMap::new())
    }

    pub fn one() -> Self {
        Self::from_hash_numerator(HashMap::from([(Unit::none(), 1.0)]))
    }

    pub fn inverse(self) -> Self {
        let mut result = Self {
            numerator: self.denominator,
            denominator: self.numerator,
        };
        result.simplify();
        result
    }

    fn all_terms(&self) -> Vec<Self> {
        let mut result = Vec::with_capacity(self.numerator.len());
        for (unit, value) in &self.numerator {
            result.push(Self {
                numerator: HashMap::from([(unit.clone(), *value)]),
                denominator: self.denominator.clone(),
            });
        }
        result
    }

    pub(crate) fn all_splits(&self) -> Vec<(Self, Self)> {
        let mut result = Vec::with_capacity(2_usize.pow(self.numerator.len() as u32) - 1);
        result.push((self.clone(), Self::zero()));

        for term in self.all_terms() {
            result.extend(
                (self.clone() - term.clone())
                    .all_splits()
                    .into_iter()
                    .map(|(k, v)| (k, v + term.clone())),
            );
        }

        result
    }

    /// Removes all `0`-values.
    /// Also simplifies divisions by singular terms.
    fn simplify(&mut self) {
        self.numerator.retain(|_, v| *v != 0.0);
        self.denominator.retain(|_, v| *v != 0.0);

        if self.denominator.len() == 1 {
            let (unit, value) = self.denominator.iter().next().unwrap();
            if unit == &Unit::none() && *value == 1.0 {
                return;
            }

            let mut result = HashMap::with_capacity(self.numerator.len());
            for (u, v) in &self.numerator {
                result.insert(u.clone() / unit, v / value);
            }
            self.numerator = result;
            self.denominator = HashMap::from([(Unit::none(), 1.0)]);
        }
    }

    fn has_denominator(&self) -> bool {
        self.denominator != HashMap::from([(Unit::none(), 1.0)])
    }
}

impl Mul<Unit> for f64 {
    type Output = NumberWithUnit;

    fn mul(self, rhs: Unit) -> Self::Output {
        let mut res = NumberWithUnit {
            numerator: HashMap::from([(rhs, self)]),
            denominator: HashMap::from([(Unit::none(), 1.0)]),
        };
        res.simplify();
        res
    }
}

// Maybe borrow other?
impl Mul<Unit> for NumberWithUnit {
    type Output = NumberWithUnit;

    fn mul(self, rhs: Unit) -> Self::Output {
        let mut result = HashMap::with_capacity(self.numerator.capacity());
        for (k, v) in self.numerator {
            result.insert(k * rhs.clone(), v);
        }

        let mut result = NumberWithUnit {
            numerator: result,
            denominator: HashMap::from([(Unit::none(), 1.0)]),
        };
        result.simplify();
        result
    }
}

impl Add for NumberWithUnit {
    type Output = NumberWithUnit;

    fn add(mut self, rhs: Self) -> Self::Output {
        if self.denominator == rhs.denominator {
            rhs.numerator.into_iter().for_each(|(u, v)| {
                self.numerator.entry(u).and_modify(|e| *e += v).or_insert(v);
            });
            self.simplify();
            self
        } else {
            self.clone()
                * NumberWithUnit {
                    numerator: rhs.denominator.clone(),
                    denominator: rhs.denominator.clone(),
                }
                + rhs
                    * NumberWithUnit {
                        numerator: self.denominator.clone(),
                        denominator: self.denominator,
                    }
        }
    }
}

impl Neg for NumberWithUnit {
    type Output = NumberWithUnit;

    fn neg(mut self) -> Self::Output {
        self.numerator.values_mut().for_each(|v| *v *= -1.0);
        self
    }
}

impl Sub for NumberWithUnit {
    type Output = NumberWithUnit;

    fn sub(self, rhs: Self) -> Self::Output {
        self + (-rhs)
    }
}

impl Div for NumberWithUnit {
    type Output = NumberWithUnit;

    // Not sure why this is disallowed here.
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, rhs: Self) -> Self::Output {
        self / (&rhs)
    }
}

impl Div<&NumberWithUnit> for NumberWithUnit {
    type Output = NumberWithUnit;

    // Not sure why this is disallowed here.
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, rhs: &Self) -> Self::Output {
        self * rhs.clone().inverse()
    }
}

impl Mul for NumberWithUnit {
    type Output = NumberWithUnit;

    fn mul(self, rhs: Self) -> Self::Output {
        self * (&rhs)
    }
}

impl Mul<&NumberWithUnit> for NumberWithUnit {
    type Output = NumberWithUnit;

    fn mul(self, rhs: &Self) -> Self::Output {
        let mut result_numerator =
            HashMap::with_capacity(self.numerator.len() * rhs.numerator.len());
        for (u1, v1) in &self.numerator {
            for (u2, v2) in &rhs.numerator {
                let unit = u1.clone() * u2;
                let value = v1 * v2;
                result_numerator
                    .entry(unit)
                    .and_modify(|e| *e += value)
                    .or_insert(value);
            }
        }
        let mut result_denominator =
            HashMap::with_capacity(self.denominator.len() * rhs.denominator.len());
        for (u1, v1) in &self.denominator {
            for (u2, v2) in &rhs.denominator {
                let unit = u1.clone() * u2;
                let value = v1 * v2;
                result_denominator
                    .entry(unit)
                    .and_modify(|e| *e += value)
                    .or_insert(value);
            }
        }
        let mut result = NumberWithUnit {
            numerator: result_numerator,
            denominator: result_denominator,
        };
        result.simplify();
        result
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn multiply_construct() {
        assert_eq!(
            NumberWithUnit::from_hash_numerator(HashMap::from([(
                Unit::base_power("km", 8) * Unit::base_power("s", -3),
                10.0
            )])),
            10.0 * Unit::base_power("km", 5)
                * Unit::base_power("s", -3)
                * Unit::base_power("km", 3)
        );
    }

    #[test]
    fn add() {
        assert_eq!(
            NumberWithUnit::from_hash_numerator(HashMap::from([
                (Unit::base_power("km", 8) * Unit::base_power("s", -3), 25.0),
                (Unit::base_power("km", 7) * Unit::base_power("s", -3), 5.0)
            ])),
            10.0 * Unit::base_power("km", 8) * Unit::base_power("s", -3)
                + 15.0 * Unit::base_power("km", 8) * Unit::base_power("s", -3)
                + 5.0 * Unit::base_power("km", 7) * Unit::base_power("s", -3)
                + 3.0 * Unit::base_power("ignore", 7) * Unit::base_power("s", -2)
                - 3.0 * Unit::base_power("ignore", 7) * Unit::base_power("s", -2)
        );
    }

    #[test]
    fn multiply() {
        assert_eq!(
            NumberWithUnit::from_hash_numerator(HashMap::from([
                (Unit::base_power("km", 8), 5.0),
                (Unit::base_power("km", 2) * Unit::base_power("s", 8), 7.0),
                (Unit::base_power("km", 6) * Unit::base_power("s", 4), 15.0),
                (Unit::base_power("s", 12), 21.0),
            ])),
            (1.0 * Unit::base_power("km", 2) + 3.0 * Unit::base_power("s", 4))
                * (5.0 * Unit::base_power("km", 6) + 7.0 * Unit::base_power("s", 8))
        );

        assert_eq!(
            NumberWithUnit::from_hash_numerator(HashMap::from([
                (Unit::base_power("km", 4), 5.0),
                (Unit::base_power("km", 2) * Unit::base_power("s", 2), 22.0),
                (Unit::base_power("s", 4), 21.0),
            ])),
            (1.0 * Unit::base_power("km", 2) + 3.0 * Unit::base_power("s", 2))
                * (5.0 * Unit::base_power("km", 2) + 7.0 * Unit::base_power("s", 2))
        );
    }

    #[test]
    fn div_simple() {
        assert_eq!(
            NumberWithUnit::from_hash_numerator(HashMap::from([(
                Unit::base_power("km", 8) * Unit::base_power("s", -3),
                10.0
            ),])),
            20.0 * Unit::base_power("km", 8) / (2.0 * Unit::base_power("s", 3))
        );
    }

    #[test]
    fn multiply_cancels() {
        assert_eq!(
            6.0 * Unit::none(),
            (2.0 * Unit::base_power("km", 2)) * (3.0 * Unit::base_power("km", -2))
        );
    }
}
