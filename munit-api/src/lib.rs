use grammar::{Command, Expr, SpannedExpr};
use mcore::{
    Conversion, EvalContext, EvalError, EvalTree, NumberWithUnit, Preferences, Simplifier, Spanned,
    Unit,
};

use rust_sitter::errors::ParseError;

type Evaluate = mcore::HeuristicBadnessEvaluator;
type Simplify = mcore::RepeatedConversionSimplifier<Evaluate>;

#[derive(Debug, thiserror::Error)]
pub enum ExecutionError {
    #[error("parsing the code failed")]
    Parse(Vec<ParseError>),
    #[error("evaluating the code failed: {0}")]
    Eval(#[from] Spanned<EvalError>),
}

#[derive(Default)]
pub struct ExecutionContext {
    eval_context: EvalContext,
    conversions: Vec<Conversion>,
    preferences: Vec<Preferences>,
}

#[derive(Debug)]
pub enum ExecutionResult {
    Number(NumberWithUnit),
    ConversionRegistered(NumberWithUnit, NumberWithUnit),
    LikeRegistered(Unit),
    DislikeRegistered(Unit),
    Exit,
    Nothing,
}

impl ExecutionContext {
    pub fn execute<S: AsRef<str>>(&mut self, code: S) -> Result<ExecutionResult, ExecutionError> {
        let ast = grammar::parse(code.as_ref()).map_err(ExecutionError::Parse)?;
        match ast.into() {
            Some(grammar::Grammar::Eval(val)) => {
                let tree = grammar_expr_to_eval_tree(val);
                let evaled = self.eval_context.eval(tree)?;
                Ok(ExecutionResult::Number(Simplify::simplify(
                    evaled,
                    &self.conversions,
                    &self.preferences,
                )))
            }
            Some(grammar::Grammar::Command(_, command)) => self.execute_command(command),
            None => Ok(ExecutionResult::Nothing),
        }
    }

    pub fn execute_command(&mut self, command: Command) -> Result<ExecutionResult, ExecutionError> {
        match command {
            Command::Exit(_) => Ok(ExecutionResult::Exit),
            Command::Conversion(_, n1, _, n2) => {
                let tree1 = grammar_expr_to_eval_tree(n1);
                let tree2 = grammar_expr_to_eval_tree(n2);
                let evaled1 = self.eval_context.eval(tree1)?;
                let evaled2 = self.eval_context.eval(tree2)?;
                self.register_equality(evaled1.clone(), evaled2.clone());
                Ok(ExecutionResult::ConversionRegistered(evaled1, evaled2))
            }
            Command::Like(_, unit) => {
                let unit = grammar_unit_to_unit(unit);
                self.register_preference(Preferences::Like(unit.clone()));
                Ok(ExecutionResult::LikeRegistered(unit))
            }
            Command::Dislike(_, unit) => {
                let unit = grammar_unit_to_unit(unit);
                self.register_preference(Preferences::Dislike(unit.clone()));
                Ok(ExecutionResult::DislikeRegistered(unit))
            }
        }
    }

    pub fn register_equality(&mut self, n1: NumberWithUnit, n2: NumberWithUnit) {
        self.conversions.push(Conversion {
            from: n1.clone(),
            to: n2.clone(),
        });
        self.conversions.push(Conversion { from: n2, to: n1 });
    }

    pub fn register_preference(&mut self, p: Preferences) {
        self.preferences.push(p);
    }
}

fn grammar_expr_to_eval_tree(t: SpannedExpr) -> EvalTree {
    let t = t.0;
    match t.value {
        Expr::Number(n) => EvalTree::Number(Spanned {
            value: grammar_number_to_number(n.value),
            span: t.span,
        }),
        // TODO: Don't hard-code operator names
        Expr::Add(a, _, b) => EvalTree::Function(Spanned {
            value: (
                "+".to_string(),
                vec![grammar_expr_to_eval_tree(*a), grammar_expr_to_eval_tree(*b)],
            ),
            span: t.span,
        }),
        Expr::Sub(a, _, b) => EvalTree::Function(Spanned {
            value: (
                "-".to_string(),
                vec![grammar_expr_to_eval_tree(*a), grammar_expr_to_eval_tree(*b)],
            ),
            span: t.span,
        }),
        Expr::Mul(a, _, b) => EvalTree::Function(Spanned {
            value: (
                "*".to_string(),
                vec![grammar_expr_to_eval_tree(*a), grammar_expr_to_eval_tree(*b)],
            ),
            span: t.span,
        }),
        Expr::Div(a, _, b) => EvalTree::Function(Spanned {
            value: (
                "/".to_string(),
                vec![grammar_expr_to_eval_tree(*a), grammar_expr_to_eval_tree(*b)],
            ),
            span: t.span,
        }),
        Expr::Bracketed(_, a, _) => grammar_expr_to_eval_tree(*a),
        Expr::Function(n, _, a, _) => EvalTree::Function(Spanned {
            value: (n, a.into_iter().map(grammar_expr_to_eval_tree).collect()),
            span: t.span,
        }),
    }
}

fn grammar_unit_to_unit(t: grammar::UnitPart) -> Unit {
    Unit::base_power(t.unit, t.power.map(|p| p.num).unwrap_or(1))
}

fn grammar_number_to_number(t: grammar::NumberWithUnit) -> NumberWithUnit {
    let value = match t.number {
        grammar::Number::Integer(i) => i as f64,
        grammar::Number::Floating(f) => f,
    };
    let unit = t
        .unit
        .map(|u| {
            u.numerator
                .units
                .into_iter()
                .map(|p| Unit::base_power(p.unit, p.power.map(|p| p.num).unwrap_or(1)))
                .fold(Unit::none(), |a, b| a * b)
                / u.denumerator
                    .map(|d| {
                        d.units
                            .units
                            .into_iter()
                            .map(|p| Unit::base_power(p.unit, p.power.map(|p| p.num).unwrap_or(1)))
                            .fold(Unit::none(), |a, b| a * b)
                    })
                    .unwrap_or(Unit::none())
        })
        .unwrap_or(Unit::none());
    value * unit
}

#[cfg(test)]
mod test;
