use mcore::{EvalError, NumberWithUnit, Spanned, Unit};

use crate::{ExecutionContext, ExecutionError, ExecutionResult};

impl ExecutionResult {
    fn number(self) -> NumberWithUnit {
        match self {
            ExecutionResult::Number(n) => n,
            _ => panic!("getting number for non-number"),
        }
    }
}

#[test]
fn simple_number() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    assert_eq!(5.0 * Unit::base("s"), api.execute("5s")?.number());
    Ok(())
}

#[test]
fn complex_number() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    assert_eq!(
        5.0 * (Unit::base_power("s", 3) * Unit::base_power("k", 2) / Unit::base_power("km", 7)),
        api.execute("5 s^3k^2|km^7")?.number()
    );
    Ok(())
}

#[test]
fn simple_add() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    assert_eq!(
        15.0 * Unit::base("s") + 25.0 * Unit::base_power("s", 2),
        api.execute("5s + 10s + 25s^2")?.number()
    );
    Ok(())
}

#[test]
fn simple_mul() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    assert_eq!(
        15.0 * Unit::base_power("s", 3) * Unit::base_power("m", 2),
        api.execute("5s * 3s^2 * 1m^2")?.number()
    );
    Ok(())
}

#[test]
fn simplify() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    api.register_equality(10.0 * Unit::base_power("m", 10), 1.0 * Unit::base("s"));
    assert_eq!(
        15.0 * Unit::base_power("s", 1),
        api.execute("150m^10")?.number()
    );
    Ok(())
}

#[test]
fn simplify_preference() -> Result<(), Box<dyn std::error::Error>> {
    let mut api = ExecutionContext::default();
    api.register_equality(1.0 * Unit::base("m"), 1.0 * Unit::base("s"));
    api.register_preference(mcore::Preferences::Like(Unit::base("s")));
    assert_eq!(
        15.0 * Unit::base_power("s", 1),
        api.execute("15m")?.number()
    );
    Ok(())
}

#[test]
fn parse_error() {
    let mut api = ExecutionContext::default();
    let res = api.execute("f(g()");
    let errs = match res {
        Ok(_) => panic!("should be error"),
        Err(ExecutionError::Eval(_)) => panic!("should be parse error"),
        Err(ExecutionError::Parse(e)) => e,
    };

    assert_eq!(errs.len(), 1);
    let err = &errs[0];
    assert!(matches!(
        &err.reason,
        rust_sitter::errors::ParseErrorReason::MissingToken(s) if s == &")".to_string()
    ));
}

#[test]
fn div_by_zero_error() {
    let mut api = ExecutionContext::default();
    let res = dbg!(api.execute("1b + 5s / 0"));
    let e = match res {
        Ok(_) => panic!("should be error"),
        Err(ExecutionError::Parse(_)) => panic!("should be eval error"),
        Err(ExecutionError::Eval(e)) => e,
    };
    assert_eq!(
        Spanned {
            value: EvalError::DivideByZeroError,
            span: (5, 11)
        },
        e
    );
}
