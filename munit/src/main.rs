use mapi::ExecutionResult;

const PREFIX: &str = ">> ";

fn error_line(start: usize, end: usize) -> String {
    let prefix_len = PREFIX.len();
    (0..(end + prefix_len))
        .map(|n| {
            if n < start + prefix_len {
                " ".to_string()
            } else if n == start + prefix_len || n == end - 1 + prefix_len {
                "^".to_string()
            } else {
                "-".to_string()
            }
        })
        .collect::<String>()
}

fn print_err(err: mapi::ExecutionError) {
    match err {
        mapi::ExecutionError::Parse(errs) => {
            for err in errs {
                println!("{}", error_line(err.start, err.end));
                match err.reason {
                    rust_sitter::errors::ParseErrorReason::UnexpectedToken(s) => {
                        println!("Unexpected token: {}.", s)
                    }
                    // TODO: Use argument?
                    rust_sitter::errors::ParseErrorReason::FailedNode(_) => {
                        println!("Failed to parse.")
                    }
                    rust_sitter::errors::ParseErrorReason::MissingToken(s) => {
                        println!("Expected token: {}.", s)
                    }
                }
            }
        }
        mapi::ExecutionError::Eval(err) => {
            println!("{}", error_line(err.span.0, err.span.1));
            let err = err.value;
            match err {
                mcore::EvalError::FunctionNotFound(f) => println!("Function not found: {}.", f),
                mcore::EvalError::IncorrectArgumentCount(f, i, j) => println!(
                    "Incorrect argument count for function {}: Expected {} but got {}.",
                    f, i, j
                ),
                mcore::EvalError::DivideByZeroError => println!("Divide by zero error."),
            }
        }
    }
}

/// Execute a line of code given the context.
/// Returns `true` if the program should stop.
fn execute<S: AsRef<str>>(api: &mut mapi::ExecutionContext, line: S) -> bool {
    let result = api.execute(line);
    match result {
        Ok(ExecutionResult::Number(number)) => println!("{}", number),
        Ok(ExecutionResult::ConversionRegistered(n1, n2)) => {
            println!("{} = {}", n1, n2)
        }
        Ok(ExecutionResult::LikeRegistered(u)) => {
            println!("Like {}", u)
        }
        Ok(ExecutionResult::DislikeRegistered(u)) => {
            println!("Dislike {}", u)
        }
        Ok(ExecutionResult::Nothing) => {}
        Ok(ExecutionResult::Exit) => return true,
        Err(e) => print_err(e),
    }

    false
}

// TODO: Better user-facing errors.
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut rl = rustyline::DefaultEditor::new()?;
    let mut api = mapi::ExecutionContext::default();

    let args = std::env::args().skip(1);

    for arg in args {
        // TODO: Maybe better function for line-by-line?
        let file = std::fs::read_to_string(arg)?;
        for line in file.lines() {
            if execute(&mut api, line) {
                return Ok(());
            }
        }
    }

    loop {
        let readline = rl.readline(PREFIX);
        match readline {
            Ok(line) => {
                if execute(&mut api, line) {
                    break;
                }
            }
            Err(rustyline::error::ReadlineError::Interrupted) => {}
            Err(_) => break,
        }
    }

    Ok(())
}
