# μnit (munit)

A simple calculator with arbitrary unit conversion.

## Example

The following provides a simple example for the usage of munit.

```
>> 1 + 2
3
>> 1s + 2s
3s
>> :convert 1s = 1000ms
1s = 1000ms
>> 1s + 2000ms
3s
>> :like ms
Like ms
>> 1s + 2000ms
3000ms
>> :convert 1i^2 = 0 - 1
1i^2 = -1
>> (1 + 2i) * (3 + 4i)
-5 + 10i
>> :exit
```

## Note

For more complex conversions, munit is not yet optimized for good performance and good results.
Therefore, expect bad results and long runtime.
